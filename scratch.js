function showTab(tabIndex) {
    const tabs = document.querySelectorAll('.tabs-title');
    const tabContents = document.querySelectorAll('.tabs-content li');

    tabs.forEach(tab => tab.classList.remove('active'));
    tabContents.forEach(content => content.classList.remove('active'));

    tabs[tabIndex].classList.add('active');
    tabContents[tabIndex].classList.add('active');
}